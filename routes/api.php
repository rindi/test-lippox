<?php
use App\Models\Client;
use App\Models\Car;
use App\Models\Rental;
use App\Http\Middleware\JsonApiMiddleware;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get("/clients", function (Request $request) {
    $clients = Client::orderBy('created_at', 'asc')->get();
    return $clients;
});

Route::post("/clients", ['middleware' => 'jsonApi', function (Request $request) {
    $validator = Validator::make($request->all(), [
        'name' => 'required|max:255',
        'gender' => 'required|max:6|gender',
    ]);

    if ($validator->fails()) {
        return new JsonResponse($validator->getMessageBag()->all(), 400);
    }

    $client = new Client();
    $client->name = $request->name;
    $client->gender = $request->gender;
    $client->save();
    $response = new stdClass();
    $response->id = $client->id;

    return new JsonResponse($response, 201);//'{"id":'.$client->id."}";
}]);

Route::put("/clients/{id}", ['middleware' => 'jsonApi', function (Request $request, $id) {
    $validator = Validator::make($request->all(), [
        'name' => 'required|max:255',
        'gender' => 'required|max:6|gender',
    ]);

    if ($validator->fails()) {
        return new JsonResponse($validator->getMessageBag()->all(), 400);
    }

    $client = Client::findOrFail($id);
    $client->name = $request->name;
    $client->gender = $request->gender;
    $client->update();

    return response(null, 200);
}])->where(['id' => '^[1-9]\d*$']);

Route::delete("/clients/{id}", function ($id) {
    Client::findOrFail($id)->delete();
    return response(null, 200);
});


Route::get("/cars", function (Request $request) {
    $clients = Car::orderBy('created_at', 'asc')->get();
    return $clients;
});

Route::post("/cars", ['middleware' => 'jsonApi', function (Request $request) {
    $validator = Validator::make($request->all(), [
        'brand' => 'required|max:255',
        'type' => 'required|max:255',
        'year' => 'required|numeric|digits:4|no_future_year',
        'color' => 'required|max:255',
        'plate' => 'required|unique:cars',
    ]);

    if ($validator->fails()) {
        return new JsonResponse($validator->getMessageBag()->all(), 400);
    }

    $car = new Car();
    $car->brand = $request->brand;
    $car->type = $request->type;
    $car->year = $request->year;
    $car->color = $request->color;
    $car->plate = $request->plate;
    $car->save();
    $response = new stdClass();
    $response->id = $car->id;

    return new JsonResponse($response, 201);//'{"id":'.$client->id."}";
}]);

Route::put("/cars/{id}", ['middleware' => 'jsonApi', function (Request $request, $id) {
    $validator = Validator::make($request->all(), [
        'brand' => 'required|max:255',
        'type' => 'required|max:255',
        'year' => 'required|numeric|digits:4|no_future_year',
        'color' => 'required|max:255',
        'plate' => 'required|unique:cars',
    ]);

    if ($validator->fails()) {
        return new JsonResponse($validator->getMessageBag()->all(), 400);
    }

    $car = Car::findOrFail($id);
    $car->brand = $request->brand;
    $car->type = $request->type;
    $car->year = $request->year;
    $car->color = $request->color;
    $car->plate = $request->plate;
    $car->update();

    return response(null, 200);
}])->where(['id' => '^[1-9]\d*$']);

Route::delete("/cars/{id}", function ($id) {
    Car::findOrFail($id)->delete();
    return response(null, 200);
});

Route::resource("/rentals", 'RentalController');

Route::get("/histories/client/{id}", function ($id) {

    $result = Client::findOrFail($id);

    $histories = [];
    foreach ($result->rentals()->get() as $rental) {
        array_push($histories, $rental->car()->first());
    }
    $result->histories = $histories;
    return response($result, 200);
})->where(['id' => '^[1-9]\d*$']);

Route::get("/histories/car/{id}", function (Request $request, $id) {
    $month = $request->query('month');
    $date_param = null;
    if (!isset($month) || ($date_param = Carbon::createFromFormat('m-Y', $month))==null) {
        return new JsonResponse(['Month format must be `MM-YYYY`'], 400);
    }

    $start_month = Carbon::create($date_param->year, $date_param->month, $date_param->day)->startOfMonth();
    $end_month = Carbon::create($date_param->year, $date_param->month, $date_param->day)->endOfMonth();

    $result = Car::where('id', '=', $id)->select('id', 'brand', 'type', 'color', 'plate')->get();
    $histories = [];
    foreach (Rental::where('car_id', '=', $id)
                 ->whereBetween('date_from', array($start_month, $end_month))
                 ->get() as $rental) {
        $client = $rental->client()->first();
        array_push($histories, (object) array(
            'rent-by'=>$client->name,
            'date-from'=> $rental->date_from,
            'date-to'=>$rental->date_to));
    }
    return response(array_merge($result->toArray(), ['histories'=> $histories]), 200);
})->where(['id' => '^[1-9]\d*$']);

Route::get("/cars/rented", function (Request $request) {
    $month = $request->query('date');
    $date_param = null;
    if (!isset($month) || ($date_param = Carbon::createFromFormat('d-m-Y', $month))==null) {
        return new JsonResponse(['Month format must be `DD-MM-YYYY`'], 400);
    }

    $histories = [];
    foreach (Rental::where('date_from', '<=', $date_param)
                 ->where('date_to', '>=', $date_param)
                 ->get() as $rental) {
        $car = $rental->car()->first();
        array_push($histories, (object) array(
            'brand'=>$car->brand,
            'type'=> $car->type,
            'plate'=>$car->plate));
    }
    return response(array_merge(['date'=>$date_param->toDateString(), 'rented_cars'=> $histories]), 200);
});

Route::get("/cars/free", function (Request $request) {
    $month = $request->query('date');
    $date_param = null;
    if (!isset($month) || ($date_param = Carbon::createFromFormat('d-m-Y', $month))==null) {
        return new JsonResponse(['Month format must be `DD-MM-YYYY`'], 400);
    }

    $histories = [];
    foreach (Rental::where('date_from', '>', $date_param)
                 ->orWhere('date_to', '<', $date_param)
                 ->get() as $rental) {
        $car = $rental->car()->first();
        array_push($histories, (object) array(
            'brand'=>$car->brand,
            'type'=> $car->type,
            'plate'=>$car->plate));
    }
    return response(array_merge(['date'=>$date_param->toDateString(), 'rented_cars'=> $histories]), 200);
});