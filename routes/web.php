<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Models\Client;
use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});

Route::get("/clients", function (Request $request) {
    $clients = Client::orderBy('created_at', 'asc')->get();
    return view('client', [
        'clients' => $clients
    ]);
});

Route::post("/clients", function (Request $request) {
    $validator = Validator::make($request->all(), [
        'name' => 'required|max:255',
        'gender' => 'required|max:6|gender',
    ]);

    if ($validator->fails()) {
        return redirect('/clients')
            ->withInput()
            ->withErrors($validator);
    }

    $client = new Client();
    $client->name = $request->name;
    $client->gender = $request->gender;
    $client->save();

    return redirect("clients");
});

Route::delete("/clients/{id}", function (Request $request, $id) {
    Client::findOrFail($id)->delete();
    return redirect('/clients');
});