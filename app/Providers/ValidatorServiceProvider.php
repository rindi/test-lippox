<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ValidatorServiceProvider extends ServiceProvider {

    public function boot()
    {
        $this->app['validator']->extend('gender', function ($attribute, $value, $parameters)
        {
            if ($value=="male" || $value=="female")
                return true;

            return false;
        });

        $this->app['validator']->extend('no_future_year', function ($attribute, $value, $parameters)
        {
            if ($value <= date("Y"))
                return true;

            return false;
        });
    }

    public function register()
    {
        //
    }
}