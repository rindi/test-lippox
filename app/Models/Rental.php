<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rental extends Model
{
    //
    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }

    public function car()
    {
        return $this->belongsTo('App\Models\Car');
    }
}
