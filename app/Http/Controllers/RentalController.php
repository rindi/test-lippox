<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Car;
use App\Models\Rental;
use App\Http\Middleware\JsonApiMiddleware;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class RentalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $rules = [
        'car_id' => 'required|numeric|exists:cars,id',
        'client_id' => 'required|numeric|exists:clients,id',
        'date_from' => 'required|date|date_format:"Y-m-d"',
        'date_to' => 'required|date|date_format:"Y-m-d"',
    ];

    public function __construct()
    {
        $this->middleware('jsonApi');
    }

    public function index()
    {
        $rentals = Rental::orderBy('created_at', 'asc')->get();
        $result = [];
        foreach ($rentals as $rental) {

            array_push($result, (object) array (
                'id'=>$rental->id,
                'name'=>$rental->client()->first()->name,
                'brand'=>$rental->car()->first()->brand,
                'type'=>$rental->car()->first()->type,
                'plate'=>$rental->car()->first()->plate,
                'date_from'=>$rental->date_from,
                'date_to'=>$rental->date_to));
        }
        return $result;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function is_valid(Request $request) {
        $err_messages = [];

        $validator = Validator::make($request->all(), $this->rules);

        if ($validator->fails()) {
            $err_messages = array_merge($err_messages, $validator->getMessageBag()->all());
        }

        $start_date = Carbon::parse($request->date_from);
        $end_date = Carbon::parse($request->date_to);
        $now = Carbon::today();
        $diff_now = $start_date->diffInDays($now);
        //    dd($start_date, $now, $diff_now);
        $diff_start_end = $end_date->diffInDays($start_date);
//        dd($start_date, $end_date, $diff_start_end);

        if ($diff_start_end > 3) {
            array_push($err_messages, 'Rented duration max 3 days');
        }

        if ($diff_now < 1 || $diff_now > 7) {
            array_push($err_messages, 'Rent date only between current day + 1 days until current date +7 days');
        }

        $current_client_rental =  DB::table('rentals')
            ->select('rentals.*')
            ->where('rentals.client_id', '=', $request->client_id)
            ->whereBetween('date_to', array($request->date_from, $request->date_to))->first();
//        dd($current_client_rental);
        if (!empty($current_client_rental)) {
            array_push($err_messages, 'Client is still rented another car at selected rent date');
        }

        $current_car_rental = DB::table('rentals')
            ->select('rentals.*')
            ->where('rentals.car_id', '=', $request->car_id)
            ->whereBetween('date_to', array($request->date_from, $request->date_to))->first();
        if (!empty($current_car_rental)) {
            array_push($err_messages, 'Car is still in rent at selected rent date');
        }

        return $err_messages;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $err_messages = $this->is_valid($request);

        if (!empty($err_messages)) {
            return new JsonResponse($err_messages, 400);
        }

        $rent = new Rental();
        $rent->car_id = $request->car_id;
        $rent->client_id = $request->client_id;
        $rent->date_from = $request->date_from;
        $rent->date_to = $request->date_to;
        $rent->save();

        return new JsonResponse(['id'=>$rent->id], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rent = Rental::findOrFail($id);
        $err_messages = $this->is_valid($request);

        if (!empty(array_filter($err_messages))) {
            return new JsonResponse($err_messages, 400);
        }

        $rent->car_id = $request->car_id;
        $rent->client_id = $request->client_id;
        $rent->date_from = $request->date_from;
        $rent->date_to = $request->date_to;
        $rent->save();

        return response(null, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Rental::findOrFail($id)->delete();
        return response(null, 200);
    }
}
