<?php

namespace App\Http\Middleware;

use Closure;

class JsonApiMiddleware
{
    const PARSED_METHODS = [
        'POST', 'PUT', 'PATCH'
    ];

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (in_array($request->getMethod(), self::PARSED_METHODS)) {
            $body = str_replace("data: ", "", $request->getContent());
            $json_object = json_decode($body, true);
            $request->merge($json_object);
        }

        return $next($request);
    }
}