@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    New Client
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('commons.errors')

                            <!-- New Task Form -->
                    <form action="{{ url('clients')}}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}

                                <!-- Task Name -->
                        <div class="form-group">
                            <label for="task-name" class="col-sm-3 control-label">Name</label>

                            <div class="col-sm-6">
                                <input type="text" name="name" id="client-name" class="form-control" value="{{ old('client') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="task-name" class="col-sm-3 control-label">Gender</label>
                            <div class="col-sm-6">
                                <input type="text" name="gender" id="client-gender" class="form-control" value="{{ old('client') }}">
                            </div>
                        </div>

                        <!-- Add Task Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <form action="{{ url('clients/') }}" method="POST">
                                    <button type="submit" class="btn btn-default">
                                        <i class="fa fa-btn fa-plus"></i>Add Client
                                    </button>
                                </form>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <!-- Current Tasks -->
            @if (count($clients) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Current Client
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped task-table">
                            <thead>
                            <th>Name</th>
                            <th>Password</th>
                            <th>&nbsp;</th>
                            </thead>
                            <tbody>
                            @foreach ($clients as $client)
                                <tr>
                                    <td class="table-text"><div>{{ $client->name }}</div></td>
                                    <td class="table-text"><div>{{ $client->gender }}</div></td>
                                    <!-- Task Delete Button -->
                                    <td>
                                        <form action="{{ url('clients/'.$client->id) }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}

                                            <button type="submit" class="btn btn-danger">
                                                <i class="fa fa-btn fa-trash"></i>Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
